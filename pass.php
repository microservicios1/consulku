<html>
<head>
  <title>Cargar CSV</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style> 
    body 
    { 
      background-image: url('idont.png'); 
      background-repeat: no-repeat; 
      background-size: 260px 70px; 
      background-position: 10% 120px; 
    } 
    button,input[type=submit],input[type=reset] 
    {
      background-color: #D6EAF8; 
      padding: 4px 4px; 
      border: outset #ABB2B9; 
      cursor: pointer; 
      font-size: 15px; 
      font-weight: bold; 
      box-shadow: 3px 4px 30px #3498DB; 
    }
    .container 
    { 
      padding: 4px 4px; 
      box-sizing: border-box; 
      font-size: 18px; 
      border:10px groove #3498DB; 
      border-radius: 25px;
    } 
  </style>
</head>
<body>
  <div class="container" align="center">
    <?php
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    $direcciones = array('DIR INFORMATICA','DIR OPERACIONES REGION 9','DIR EJECUTIVA DE UNIDADES REGIONALES','DIR RECURSOS HUMANOS','DIR SVA & IOT','DIR FINANZAS Y ADMINISTRACION','DIR INGENIERIA Y CALIDAD DE RED','DIR TRANSFORMACION DIGITAL','DIR OPERACION Y MANTENIMIENTO');
    $gerencias = array('GER PLACEHOLDER1','GER PLACEHOLDER2','GER PLACEHOLDER3');
    $fname = $_FILES['somefile']['name'];
    if($fname)
      echo 'Cargando archivo : '.$fname.' <br> ';
    $exte = explode(".",$fname);
    if(strtolower(end($exte))=="csv")
    {
      $filename = $_FILES['somefile']['tmp_name'];
      $handle =fopen($filename,"r");
      $control=0;
      $line=0;
      $proyectosArray = array();
      $h2="";
      $proyectoActual="";
      mysqli_autocommit($conn, FALSE);
      mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
      while(($dataCSV = fgetcsv($handle,5000,","))!==FALSE)
      {
        $line++;
        //   Quitar acentos
          for($i=0;$i<41;$i++)
          {
            $dataCSV[$i]=str_replace('\'','`',$dataCSV[$i]);
            $dataCSV[$i]=str_replace('\"','``',$dataCSV[$i]);
            if((($i>9&&$i<13)||($i>15&&$i<19))&&!is_numeric($dataCSV[$i]))
              $dataCSV[$i]=0;
          }
        //   Definir Folio
          $proyecto = str_replace(' ', '_', $dataCSV[7]);
          $proyecto = str_replace(',', '_', $proyecto);
          $proyecto = str_replace('.', '_', $proyecto);
          $dataCSV[1]=str_replace('/','-',$dataCSV[1]);
          $dataCSV[2]=str_replace(' ', '_', $dataCSV[2]);
          $folio=substr($proyecto,0,10)."_".substr($proyecto,-10)."_".$dataCSV[1]."_".$dataCSV[2];
        //   Revisar si hay cambio de proyecto
          if(!isset($proyectosArray[$folio]))
          {
            $direccionID=100;
            for($j=0;$j<sizeof($direcciones);$j++)
            {
              if($dataCSV[4]==$direcciones[$j])
              $direccionID=$j+1;
            }
            $gerenciaID=100;
            for($j=0;$j<sizeof($gerencias);$j++)
            {
              if($dataCSV[3]==$gerencias[$j])
              $gerenciaID=$j+1;
            }
            if($direccionID<=4)
              $admin="RODRIGO LOPEZ MARTINEZ";
            else if ($direccionID<=10)
              $admin="DIEGO ARMANDO MARTINEZ RODRIGUEZ";
            else
              $admin="POR DEFINIR";
            $maquina=0;
            $sql="insert into proyectos (folio,fecha,gerenciaId,direccionId,proyecto,diagrama,comentarios,descripcion,solicita,administra) values ('".$folio."','".$dataCSV[1]."',".$gerenciaID.",".$direccionID.",'".$proyecto."','','','Importado desde archivo CSV','".$dataCSV[2]."','".$admin."')";
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
            {
              if($control==0)
                echo "<br>Fallo: ".$sql;
              $control=99;
              $h2=$h2."<br>".$line;
            }
            $proyectosArray[$folio]=0;
          }
          else
            $proyectosArray += [$folio => 0];
        //   Crear insert de maquina
          $interId=$folio.$proyectosArray[$folio];
          $dataCSV[21]=str_replace('/','-',$dataCSV[21]);
          $dataCSV[28]=str_replace('/','-',$dataCSV[28]);
          $dataCSV[29]=str_replace('/','-',$dataCSV[29]);
          if($dataCSV[39]=="TD")
            $dataCSV[39]=1;
          if($dataCSV[39]=="OYM")
            $dataCSV[39]=2;
          else
            $dataCSV[39]=0;
          $sql="insert into maquinas (folio,interId,arreglo,tipo,aplicacion,ambienteSolicitado,ambienteEntregado,CPUSolicitado,RAMSolicitado,storageSolicitado,sharedSolicitado,CPUEntregado,RAMEntregado,storageEntregado,sharedEntregado,nombre,ip,infraestructuraDef,entregaUser,estatus,detalleEstatus,estatusOYM,remedy,fechaSoliMOP,fechaEntregaServer,TDOYM) values ('".$folio."','".$interId."',0,'Standalone','".$dataCSV[8]."','".$dataCSV[5]."','".$dataCSV[6]."',".$dataCSV[10].",".$dataCSV[11].",".$dataCSV[12].",0,".$dataCSV[16].",".$dataCSV[17].",".$dataCSV[18].",0,'".$dataCSV[22]."','".$dataCSV[23]."','".$dataCSV[30]."','".$dataCSV[21]."','".$dataCSV[24]."','".$dataCSV[25]."','".$dataCSV[26]."','".$dataCSV[27]."','".$dataCSV[28]."','".$dataCSV[29]."',".$dataCSV[39].")";
          mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r<1)
          {
            if($control==0)
              echo "<br>Fallo: ".$sql;
            $control=99;
            $h2=$h2."<br>".$line;
          }
        //   Agregar especificaciones a SO
          $sql="insert into especificacionesSO (folioNumber,folio,SOSolicitado,SOEntregado,kernel) values ('".$interId."','".$folio."','".$dataCSV[13]."','".$dataCSV[19]."','Subido CSV , actualizar info kernel')";
          mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r<1)
          {
            if($control==0)
              echo "<br>Fallo: ".$sql;
            $control=99;
            $h2=$h2."<br>".$line;
          }
        //   agregar especificaciones a DB
          $sql="insert into especificacionesDB (folioNumber,folio,DBSolicitada,DBEntregado) values ('".$interId."','".$folio."','".$dataCSV[20]."','".$dataCSV[39]."')";
          mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r<1)
          {
            if($control==0)
              echo "<br>Fallo: ".$sql;
            $control=99;
            $h2=$h2."<br>".$line;
          }
          $proyectosArray[$folio]++;
      }
      if($control!=0)
      {
        echo 'fallo en la linea <br> '.$h2;
        mysqli_rollback($conn);
      }
      if($control==0)
      {
        echo "<p> importacion exitosa </p>";
        mysqli_commit($conn);
      }
      mysqli_close($conn);
    }
    else
      echo "<p>Archivo no existente o incompatible</p>";
    ?>
    <form action="<?php echo $inside; ?>">
      <input type="submit" value="Regresar" >
    </form>
  </div>
</body>
</html>